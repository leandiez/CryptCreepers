using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HUD_Manager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI killsText, remainingTimeText, hpText, finalScoreText;
    [SerializeField] GameObject gameOverScreen;
    public static HUD_Manager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Update()
    {
        killsText.text = "Kills: " + GameManager.instance.Score;
        remainingTimeText.text = "Time: " + GameManager.instance.time;
    }

    public void UpdatePlayerHP(int newHP)
    {
        hpText.text = "HP: " + newHP;
    }

    public void ShowGameOverScreen()
    {
        finalScoreText.text = "Final Score:\n" + GameManager.instance.Score;
        gameOverScreen.SetActive(true);
    }


}
