using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] int score;

    public int Score
    {
        get => score;
        set
        {
            score = value;
            if (score % 1000 == 0)
            {
                gameDifficult++;
            }
        }
    }

    public static GameManager instance;
    public bool gameIsRunning = true;
    public int gameDifficult = 1;
    public int time = 30;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }
    private void Start()
    {
        gameIsRunning = true;
        StartCoroutine(CountdownRuntime());
    }

    public void GameOver()
    {
        gameIsRunning = false;
        HUD_Manager.instance.ShowGameOverScreen();
    }

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    IEnumerator CountdownRuntime()
    {
        while (time > 0)
        {
            yield return new WaitForSeconds(1);
            time--;
        }
        GameOver();
    }

}
